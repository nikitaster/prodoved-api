Документация разработчика
=========================

************
View–функции
************

.. automodule:: API_App.views
    :members:
    :show-inheritance:

**********************
Модели для базы данных
**********************

.. automodule:: API_App.models
    :members:

*************************
Rest framework serializer
*************************

.. automodule:: API_App.serializer
    :members:
    :show-inheritance:
