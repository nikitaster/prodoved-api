from django.http import HttpResponse
from rest_framework import generics, permissions
from rest_framework.authentication import TokenAuthentication, SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status

from API_App.models import *
from API_App.permissions import IsOwnerOrReadOnly
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from API_App.serializer import *
from Modules.BarcodeDetector import BarcodeDetector
from Modules.ImageController import ImageController

from django.contrib.auth.models import User

from PIL import Image
import imagehash
import requests


# base rest views classes
class BaseCreateView(generics.CreateAPIView):
    serializer_class = None
    # permission_classes = (IsAdminUser, )
    # permission_classes = (IsOwnerOrReadOnly, )
    permission_classes = (permissions.DjangoModelPermissions, )
    queryset = Goods.objects.all()


class BaseListView(generics.ListAPIView):
    serializer_class = None
    queryset = []
    authentication_classes = (TokenAuthentication, SessionAuthentication, BasicAuthentication)
    # permission_classes = (IsAuthenticated, )
    permission_classes = (permissions.DjangoModelPermissions, )


class BaseDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = None
    queryset = []
    authentication_classes = (TokenAuthentication, SessionAuthentication, BasicAuthentication)
    # permission_classes = (IsOwnerOrReadOnly, )
    # permission_classes = (IsAdminUser, )
    permission_classes = (permissions.DjangoModelPermissions,)


# goods rest view classes
class GoodsCreateView(BaseCreateView):
    serializer_class = GoodsDetailSerializer


class GoodsListView(BaseListView):
    serializer_class = GoodsListSerializer
    queryset = Goods.objects.all()


class GoodsDetailView(BaseDetailView):
    serializer_class = GoodsDetailSerializer
    queryset = Goods.objects.all()


# moderation goods rest view classes
class GoodsOnModerationCreateView(BaseCreateView):
    serializer_class = ModerationGoodsDetailSerializer


class GoodsOnModerationListView(BaseListView):
    serializer_class = ModerationGoodsListSerializer
    queryset = GoodsOnModeration.objects.all()


class GoodsOnModerationDetailView(BaseDetailView):
    serializer_class = ModerationGoodsDetailSerializer
    queryset = GoodsOnModeration.objects.all()


# category rest view classes
class CategoryCreateView(BaseCreateView):
    serializer_class = CategoryDetailSerializer


class CategoryListView(BaseListView):
    serializer_class = CategoryListSerializer
    queryset = Category.objects.all()


class CategoryDetailView(BaseDetailView):
    serializer_class = CategoryDetailSerializer
    queryset = Category.objects.all()


# pictures rest view classes
class PictureCreateView(BaseCreateView):
    serializer_class = PictureDetailSerializer


class PictureListView(BaseListView):
    serializer_class = PictureListSerializer
    queryset = Picture.objects.all()


class PictureDetailView(BaseDetailView):
    serializer_class = PictureDetailSerializer
    queryset = Picture.objects.all()


# negative characteristics rest view classes
class NegativeCreateView(BaseCreateView):
    serializer_class = NegativeDetailSerializer


class NegativeListView(BaseListView):
    serializer_class = NegativeListSerializer
    queryset = Negative.objects.all()


class NegativeDetailView(BaseDetailView):
    serializer_class = NegativeDetailSerializer
    queryset = Negative.objects.all()


# positive characteristics rest view classes
class PositiveCreateView(BaseCreateView):
    serializer_class = PositiveDetailSerializer


class PositiveListView(BaseListView):
    serializer_class = PositiveListSerializer
    queryset = Positive.objects.all()


class PositiveDetailView(BaseDetailView):
    serializer_class = PositiveDetailSerializer
    queryset = Positive.objects.all()


# any
class GetByBarCode(generics.ListAPIView):
    serializer_class = GoodsListSerializer
    queryset = Goods.objects.none()
    permission_classes = (permissions.DjangoModelPermissions, )

    def get(self, request, barcode):
        queryset = Goods.objects.filter(barcode=barcode)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class SearchProduct(generics.ListAPIView):
    serializer_class = GoodsListSerializer
    queryset = Goods.objects.all()
    permission_classes = (permissions.DjangoModelPermissions, )

    def get(self, request):
        queryset = {'status': None}
        target_good = None
        picture = Picture(file=request.FILES['file'],
                          platform=request.GET.get('platform'),
                          hash=imagehash.average_hash(Image.open(request.FILES['file'])),
                          author=request.GET.get('author'),
                          )

        # ИЩЕМ ПО ХЭШУ
        hashes_list = list(Picture.objects.values_list('hash', flat=True))
        own_hash = imagehash.average_hash(Image.open(request.FILES['file']))
        queryset['image_hash'] = str(own_hash)
        if str(own_hash) in hashes_list:
            picture = Picture.objects.get(hash=own_hash)
            if picture.target_good:
                queryset['status'] = 'ok'
                queryset['good_name'] = picture.target_good.name
                queryset['image_hash'] = str(picture.hash)
                queryset['detected_by'] = 'hash'
                return Response(queryset)

        # ПОЛУЧЕНИЕ КАРТИНКИ ПОЛЬЗОВАТЕЛЯ
        image_controller = ImageController()
        # СОХРАНЕНИЕ КАРТИНКИ ПОЛЬЗОВАТЕЛЯ ДЛЯ ОБРАБОТКИ
        image_controller.save(request_file=request.FILES['file'])
        # ИЩЕМ БАРКОД
        barcode_detector = BarcodeDetector()
        bar = barcode_detector.detect('collectedmedia/{}'.format(image_controller.get_file_name()))
        if bar and Goods.objects.filter(barcode=bar[0]['barcode']):
            target_good = Goods.objects.get(barcode=bar[0]['barcode'])
            picture.target_good = target_good
            picture.save()
            queryset['status'] = 'ok'
            queryset['good_name'] = target_good.name
            queryset['image_hash'] = str(picture.hash)
            queryset['detected_by'] = 'barcode'
        else:
            # запрос к нейронке
            picture.save()
            response_ml = requests.request('GET', 'http://api.scanner.savink.in:5000', files={'image': picture.file})
            if response_ml.status_code == 200:
                response_json = response_ml.json()
                if response_json:
                    model_name = response_json['model_name']
                    if Goods.objects.filter(imageAIname=model_name):
                        target_good = Goods.objects.get(imageAIname=model_name)
                        picture.target_good = target_good
                        picture.save()
                        queryset['status'] = 'ok'
                        queryset['good_name'] = target_good.name
                        queryset['image_hash'] = str(picture.hash)
                        queryset['detected_by'] = 'neural_network'

        # УДАЛЕНИЕ КАРТИНКИ ПОЛЬЗОВАТЕЛЯ ПОСЛЕ ОБРАБОТКИ
        image_controller.delete_image()
        return Response(queryset)


class GetBarCode(generics.ListAPIView):
    serializer_class = None
    queryset = None
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        queryset = {'status': None}
        image_controller = ImageController()
        barcode_detector = BarcodeDetector()

        image_controller.save(request_file=request.FILES['file'])
        bar = barcode_detector.detect('collectedmedia/{}'.format(image_controller.get_file_name()))

        if bar:
            bar = bar[0]['barcode']

            queryset['status'] = 'ok'
            queryset['barcode'] = bar

        image_controller.delete_image()

        return Response(queryset)


class GetGoodByName(generics.ListAPIView):
    serializer_class = GoodsListSerializer
    queryset = Goods.objects.none()
    permission_classes = (permissions.DjangoModelPermissions, )

    def get(self, request, name):

        # queryset = Goods.objects.filter(name=name)
        # serializer = self.serializer_class(queryset, many=True)
        # return Response(serializer.data)
        try:
            good = Goods.objects.get(name=name)

            queryset = {}

            positives_q = Positive.objects.filter(good=good)
            negatives_q = Negative.objects.filter(good=good)

            positives = []
            negatives = []

            for item in positives_q:
                positives.append(item.value)
            for item in negatives_q:
                negatives.append(item.value)

            queryset['id'] = good.id
            queryset['name'] = good.name
            queryset['barcode'] = good.barcode
            queryset['points'] = good.points_rusControl
            queryset['positives'] = positives
            queryset['negatives'] = negatives
            queryset['image'] = good.file.url

            categories = good.category.get_ancestors(include_self=True)
            categories_list = []
            for category in categories:
                categories_list.append((category.url_name, category.name))
            queryset['categories'] = categories_list
        except Exception:
            queryset = []

        return Response(queryset)


class CategoryFilterByName(BaseListView):
    serializer_class = CategoryListSerializer
    queryset = Category.objects.none()

    def get(self, request, name):
        queryset = Category.objects.get(url_name=name).get_children()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class GetGoodByCategory(BaseListView):
    serializer_class = GoodsListSerializer
    queryset = Goods.objects.none()

    def get(self, request, category_name):
        if Category.objects.filter(url_name=category_name):
            category = Category.objects.get(url_name=category_name)
            queryset = Goods.objects.filter(category=category)
            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response(self.queryset)


class GetPictureByHash(BaseListView):
    serializer_class = PictureListSerializer
    queryset = Picture.objects.none()

    def get(self, request, own_hash):
        if Picture.objects.filter(hash=own_hash):
            queryset = Picture.objects.get(hash=own_hash)
            serializer = self.serializer_class(queryset, many=False)
            return Response(serializer.data)
        else:
            return Response(self.queryset)


class DownloadPictureByHash(BaseListView):
    serializer_class = PictureListSerializer
    queryset = Picture.objects.none()

    def get(self, request, own_hash):
        if Picture.objects.filter(hash=own_hash):
            queryset = Picture.objects.get(hash=own_hash)
            # serializer = self.serializer_class(queryset, many=False)
            return HttpResponse(queryset.file, content_type="image/png")
        else:
            return Response(self.queryset)


class GoodsOnModerationCreateWithHashImage(BaseDetailView):
    serializer_class = ModerationGoodsDetailSerializer
    queryset = GoodsOnModeration.objects.none()

    def post(self, request):
        own_hash = request.POST.get('hash')
        picture = Picture.objects.get(hash=own_hash)
        new_good = GoodsOnModeration(
            name=request.POST.get('name'),
            image=picture,
            barcode=request.POST.get('barcode'),
            user=request.user
        )
        new_good.save()
        serializer = self.serializer_class(new_good, many=False)
        return Response(serializer.data)


class GetPictureListByGoodName(BaseListView):
    serializer_class = PictureListSerializer
    queryset = Picture.objects.none()

    def get(self, request, good_name):
        if Goods.objects.filter(name=good_name):
            good = Goods.objects.get(name=good_name)
            queryset = Picture.objects.filter(target_good=good)
            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data)
        return Response(self.queryset)


class GoodsOnModerationGetListByStatus(BaseListView):
    serializer_class = ModerationGoodsListSerializer
    queryset = GoodsOnModeration.objects.none()

    def get(self, request, status):
        queryset = GoodsOnModeration.objects.filter(status=status)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class GoodsAcceptView(BaseDetailView):
    serializer_class = GoodsDetailSerializer
    queryset = Goods.objects.none()

    def post(self, request):
        new_good = Goods(
            name=request.POST.get('name'),
            file=Picture.objects.get(id=request.POST.get('file')).file,
            barcode=request.POST.get('barcode'),
            points_rusControl=request.POST.get('points_rusControl'),
            user=request.user,
            category=Category.objects.get(id=request.POST.get('category'))
        )
        new_good.save()
        serializer = self.serializer_class(new_good, many=False)
        return Response(serializer.data)


class GoodsAllNames(BaseListView):
    serializer_class = GoodsListSerializer
    queryset = Goods.objects.none()

    def get(self, request):
        data = Goods.objects.values_list('name', flat=True)
        return Response(data)


class CategoryAllNames(BaseListView):
    serializer_class = CategoryListSerializer
    queryset = Category.objects.none()

    def get(self, request):
        data = Category.objects.values_list('name', 'url_name', flat=False)
        return Response(data)


class PictureAmountView(BaseListView):
    serializer_class = PictureListSerializer
    queryset = Picture.objects.none()

    def get(self, request):
        data = Picture.objects.all().count()
        return Response(data)


class AddPictureToGood(BaseDetailView):
    serializer_class = PictureDetailSerializer
    queryset = Picture.objects.none()

    def post(self, request):
        try:
            image = request.FILES.get('image')
            hash = imagehash.average_hash(Image.open(image))

            new_picture = Picture(
                file=image,
                hash=hash,
                author=request.POST.get('author'),
                target_good=Goods.objects.get(name=request.POST.get('good')),
                platform=request.POST.get('platform'),
            )

            if Picture.objects.filter(hash=hash):
                return Response({'status': 'error', 'msg': 'this hash already uploaded'})

            new_picture.save()
            return Response({'status': 'complete'})

        except Exception as ex:
            return Response({'status': 'error', 'msg': '{}'.format(ex)})


class AddPictureToGood(BaseDetailView):
    serializer_class = PictureDetailSerializer
    queryset = Picture.objects.none()

    def post(self, request):
        try:
            image = request.FILES.get('image')
            hash = imagehash.average_hash(Image.open(image))

            new_picture = Picture(
                file=image,
                hash=hash,
                author=request.POST.get('author'),
                target_good=Goods.objects.get(name=request.POST.get('good')),
                platform=request.POST.get('platform'),
            )

            if Picture.objects.filter(hash=hash):
                return Response({'status': 'error', 'msg': 'this hash already uploaded'})

            new_picture.save()
            return Response({'status': 'complete'})

        except Exception as ex:
            return Response({'status': 'error', 'msg': '{}'.format(ex)})

