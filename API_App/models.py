"""
Моодели для создания таблиц в базе данных
"""
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

User = get_user_model()


class Category(MPTTModel):
    """
    Класс для создания категорий. Поля: name(название категории), url_name(url-ссылка для
    категории), parent(родитель категории), created(дата создания), updated(дата изменения),
    user(администратор, создавший категорию), file(картинка, принадлежащая категории).
    Отнаследован от MPTTModel из модуля mptt.
    """
    name = models.CharField(verbose_name='Наименование', max_length=128, unique=True)
    url_name = models.CharField(verbose_name='url name', max_length=128, unique=True, default=None,
                                null=True)
    parent = TreeForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True,
                            related_name='children')
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Обновлено', auto_now=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL,
                             null=True)
    file = models.FileField(verbose_name='Ссылка на s3 хранилище', upload_to='photos', null=True)

    class MPTTMeta:
        """
        Мета-класс из модуля mttp  для настройки категорий
        """
        level_attr = 'level'
        order_insertion_by = ['name']
        parent_attr = 'parent'
        left_attr = 'lft'
        right_attr = 'rght'
        tree_id_attr = 'tree_id'


class Goods(models.Model):
    """
    Класс для создания модели товара. Поля: name(название товара), barcode(штрих-код товара),
    imageAIname(название товара в нейронной сети), category(категория, к которой принадлежит
    товар), created(дата создания товара), updated(дата обновления информации о товаре),
    user(модератор или алминистратор, добавивший товар), file(картинка, принадлежащая товару),
    points_rusControl(оценка товара командой Продовед). Отнаследован от базового класса Django
    models.Model
    """
    name = models.CharField(verbose_name='Наименование', db_index=True, max_length=128,
                            unique=True)
    barcode = models.TextField(verbose_name='Штрих-код', db_index=True, default=None, null=True)
    imageAIname = models.CharField(verbose_name='Имя в модели нейросети', db_index=True,
                                   max_length=64, default=None, null=True)
    category = models.ForeignKey(Category, verbose_name='Категория',
                                 on_delete=models.SET_NULL, default=None, null=True)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Обновлено', auto_now=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL,
                             null=True)
    file = models.ImageField(verbose_name='Ссылка на s3 хранилище', upload_to='photos', null=True)
    points_rusControl = models.CharField(verbose_name='Оценка Росконтроля',
                                         max_length=10, default='Не указано', null=True)

    def get_positives(self):
        """
        Функция для получения позитивных свойств товара. Использутеся в отображении информации о
        товаре на его странице

        :return: QuerySet с положительными свойствами товара
        :rtype: :class:`QuerySet`
        """
        return Positive.objects.filter(good=self)

    def get_negatives(self):
        """
        Функция для получения отрицательных свойств товара. Использутеся в отображении информации о
        товаре на его странице

        :return: QuerySet с отрицательными свойствами товара
        :rtype: :class:`QuerySet`
        """
        return Negative.objects.filter(good=self)

    def get_comments(self):
        """
        Функция для получения комментариев к товару

        :return: QuerySet с комментариями к товару
        :rtype: :class:`QuerySet`
        """
        return Comment.objects.filter(good=self)


class Picture(models.Model):
    """
    Класс для модели картинки. Поля: file(ссылка на файл картинки в S3 хранилище), hash(хэш
    картинки), author(автор картинки), created(дата создания), target_good(товар, которому
    принадлежит картинка), platform(платформа, с которой загружена картинка). Отнаследован от
    базового класса Django models.Model
    """
    file = models.FileField(verbose_name='Ссылка на s3 хранилище', upload_to='photos')
    hash = models.TextField(verbose_name='Хэш фото', null=True, unique=True)
    # user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE)
    author = models.CharField(verbose_name='Загрузил', null=True, max_length=32,
                              default='Не указан')
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    target_good = models.ForeignKey(Goods, verbose_name='Товар', on_delete=models.CASCADE,
                                    null=True, default=None)
    platform = models.TextField(verbose_name='Платформа', default='Неизвестная платформа')


class Positive(models.Model):
    """
    Класс для создания модели положительных свойств товара. Поля: good(товар, которому
    принадлежит свойство), value(описание свойства), user(администратор, добавивший свойство),
    created(дата создания), updated(дата изменения информации). Отнаследован от базового класса
    Django models.Model
    """
    good = models.ForeignKey(to=Goods, verbose_name='Продукт', on_delete=models.CASCADE)
    value = models.CharField(verbose_name='Достоинство', max_length=128)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL,
                             null=True)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Обновлено', auto_now=True)


class Negative(models.Model):
    """
    Класс для создания модели отрицательных свойств товара. Поля: good(товар, которому
    принадлежит свойство), value(описание свойства), user(администратор, добавивший свойство),
    created(дата создания), updated(дата изменения информации). Отнаследован от базового класса
    Django models.Model
    """
    good = models.ForeignKey(to=Goods, verbose_name='Продукт', on_delete=models.CASCADE)
    value = models.CharField(verbose_name='Недостаток', max_length=128)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL,
                             null=True)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Обновлено', auto_now=True)


class GoodsOnModeration(models.Model):
    """
    Класс для временного хранения товаров на модерации. Поля: name(название товара на
    модерации), image(картинка, принадлежащая товарц), barcode(штрих-код на товаре), STATUSES(
    список статусов модерации), status(текущий статус модерации), user(пользователь, отправивший
    товар на модерацию), created(дата создания). Отнаследован от базового класса Django
    models.Model
    """
    name = models.TextField(verbose_name='Наименование', max_length=255)
    image = models.ForeignKey(Picture, verbose_name='Изображение', on_delete=models.SET_NULL,
                              null=True)
    barcode = models.TextField(verbose_name='Штрих-код', db_index=True, default=None, null=True)
    STATUSES = [
        ('Принято на модерацию', 'Принято на модерацию'),
        ('Одобрено', 'Одобрено'),
        ('Отклонено', 'Отклонено'),
    ]
    status = models.CharField(verbose_name='Статус', max_length=25,
                              choices=STATUSES, default='Принято на модерацию')
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL,
                             null=True)
    created = models.DateTimeField(auto_now_add=True)
