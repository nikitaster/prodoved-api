"""
Стандартный файл Rest framework для сериализации классов
"""
from rest_framework import serializers

from API_App.models import Category, Goods, GoodsOnModeration, Negative, Picture, Positive


class BaseDetailSerializer(serializers.ModelSerializer):
    """
    Базовый класс для детализации объекта в базе данных. Поле: user(дефолтный пользователь для
    действий). Отнаследован от базового класса Rest framework serializers.ModelSerializer
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())


class BaseListSerializer(serializers.ModelSerializer):
    """
    Базовый класс для отображения списка объектов из базе данных. Поле: None. Отнаследован от
    базового класса Rest framework serializers.ModelSerializer
    """
    pass


# For goods
class GoodsDetailSerializer(BaseDetailSerializer):
    """
    Класс для детализации товара из базы данных. Поля: None. Отнаследован от базового класса
    BaseDetailSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Goods
        """
        model = Goods
        fields = '__all__'
        # fields = ('id', 'name', 'user')


class GoodsListSerializer(BaseListSerializer):
    """
    Класс для отображения списка товаров из базы данных. Поля: None. Отнаследован от базового
    класса BaseListSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Goods
        """
        model = Goods
        fields = '__all__'


# For moderation goods
class ModerationGoodsDetailSerializer(BaseDetailSerializer):
    """
    Класс для детализации товара на модерации из базы данных. Поля: None. Отнаследован от
    базового класса BaseDetailSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели GoodsOnModeration
        """
        model = GoodsOnModeration
        fields = '__all__'
        # fields = ('id', 'name', 'user')


class ModerationGoodsListSerializer(BaseListSerializer):
    """
    Класс для отображения списка товаров на модерации из базы данных. Поля: None. Отнаследован от
    базового класса BaseListSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели GoodsOnModeration
        """
        model = GoodsOnModeration
        fields = '__all__'


#  for pictures
class PictureDetailSerializer(BaseDetailSerializer):
    """
    Класс для детализации картинки из базы данных. Поля: None. Отнаследован от
    базового класса BaseDetailSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Picture
        """
        model = Picture
        fields = '__all__'
        # fields = ('id', 'name', 'user')


class PictureListSerializer(BaseListSerializer):
    """
    Класс для отображения списка картинок из базы данных. Поля: None.
    Отнаследован от базового класса BaseListSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Picture
        """
        model = Picture
        fields = '__all__'


# for categories
class CategoryDetailSerializer(BaseDetailSerializer):
    """
    Класс для детализации категории из базы данных. Поля: None. Отнаследован от
    базового класса BaseDetailSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Category
        """
        model = Category
        fields = '__all__'
        # fields = ('id', 'name', 'user')


class CategoryListSerializer(BaseListSerializer):
    """
    Класс для отображения списка категорий из базы данных. Поля: None.
    Отнаследован от базового класса BaseListSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Category
        """
        model = Category
        fields = '__all__'


# for positive characteristics
class PositiveDetailSerializer(BaseDetailSerializer):
    """
    Класс для детализации положительного качества товара из базы данных. Поля: None.
    Отнаследован от базового класса BaseDetailSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Positive
        """
        model = Positive
        fields = '__all__'
        # fields = ('id', 'name', 'user')


class PositiveListSerializer(BaseListSerializer):
    """
    Класс для отображения списка положительных качеств товара из базы данных. Поля: None.
    Отнаследован от базового класса BaseListSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Positive
        """
        model = Positive
        fields = '__all__'


# for negative characteristics
class NegativeDetailSerializer(BaseDetailSerializer):
    """
    Класс для детализации отрицательного качества товара из базы данных. Поле: user(дефолтный
    пользователь для действий). Отнаследован от базового класса BaseDetailSerializer
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        """
        Мета класс для отображения всех полей модели Negative
        """
        model = Negative
        fields = '__all__'
        # fields = ('goods', 'value')

    def create(self, validated_data):
        """
        Функция для создания отрицательного качества товара

        :param validated_data: Данные для создания модели Negative
        :type validated_data: :class:`dict`
        :return: Объект Negative
        :rtype: :class:`API_App.models.Negative`
        """
        return Negative.objects.create(**validated_data)


class NegativeListSerializer(BaseListSerializer):
    """
    Класс для отображения списка отрицательных качеств товара из базы данных. Поля: None.
    Отнаследован от базового класса BaseListSerializer
    """
    class Meta:
        """
        Мета класс для отображения всех полей модели Negative
        """
        model = Negative
        fields = '__all__'
