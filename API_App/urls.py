"""Scanner URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from API_App.views import *

urlpatterns = [
    path('goods/create/', GoodsCreateView.as_view()),
    path('goods/accept/', GoodsAcceptView.as_view()),
    path('goods/all/', GoodsListView.as_view()),
    path('goods/all_names/', GoodsAllNames.as_view()),
    path('goods/detail/<int:pk>/', GoodsDetailView.as_view()),
    path('goods/get_by_name/<str:name>/', GetGoodByName.as_view()),
    path('goods/get_by_category/<str:category_name>/', GetGoodByCategory.as_view()),

    path('category/create/', CategoryCreateView.as_view()),
    path('category/all/', CategoryListView.as_view()),
    path('category/detail/<int:pk>/', CategoryDetailView.as_view()),
    path('category/filter/<str:name>/', CategoryFilterByName.as_view()),
    path('category/all_names_urls/', CategoryAllNames.as_view()),

    path('picture/create/', PictureCreateView.as_view()),
    path('picture/all/', PictureListView.as_view()),
    path('picture/get_amount/', PictureAmountView.as_view()),
    path('picture/detail/<int:pk>/', PictureDetailView.as_view()),
    path('picture/get_pictures_list_by_good_name/<str:good_name>',
         GetPictureListByGoodName.as_view()),
    path('picture/get_picture_by_hash/<str:own_hash>', GetPictureByHash.as_view()),
    path('picture/download_picture_by_hash/<str:own_hash>', DownloadPictureByHash.as_view()),
    path('picture/add-to-product/', AddPictureToGood.as_view()),

    path('negative/create/', NegativeCreateView.as_view()),
    path('negative/all/', NegativeListView.as_view()),
    path('negative/detail/<int:pk>/', NegativeDetailView.as_view()),

    path('positive/create/', PositiveCreateView.as_view()),
    path('positive/all/', PositiveListView.as_view()),
    path('positive/detail/<int:pk>/', PositiveDetailView.as_view()),

    path('goods_on_moderation/create/', GoodsOnModerationCreateView.as_view()),
    path('goods_on_moderation/create_by_hash/', GoodsOnModerationCreateWithHashImage.as_view()),
    path('goods_on_moderation/all/', GoodsOnModerationListView.as_view()),
    path('goods_on_moderation/detail/<int:pk>/', GoodsOnModerationDetailView.as_view()),
    path('goods_on_moderation/get_list_by_status/<str:status>/',
         GoodsOnModerationGetListByStatus.as_view()),

    path('goods/barcode/<str:barcode>/', GetByBarCode.as_view()),
    path('goods/get_product/', SearchProduct.as_view()),

    path('getbarcode/', GetBarCode.as_view()),
]
